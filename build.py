#!/usr/bin/python

import subprocess, sys

name = sys.argv[1] if len(sys.argv) == 2 else "wissarbIEEE-template"
commands = [
    ["pdflatex", name + ".tex"],
    ["bibtex", name + ".aux"],
    ["pdflatex", name + ".tex"],
    ["pdflatex", name + ".tex"]
]

for c in commands:
    subprocess.call(c)